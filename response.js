var tmp_o = {
    "사칙연산" : ["덧셈", "뺄셈", "곱셈", "나눗셈"],
    "요일" : ["월요일", "화요일", "수요일", "목요일", "금요일", "토요일", "일요일"],
    "자음" : ["기역", "니은", "디귿", "리을", "미음", "비읍", "시옷", "이응", "지읒", "치읓", "키읔", "티읕", "피읖", "히읗"],
};

var tmp_c = {};

function getQuiz(sub)
{
    if(!tmp_o[sub])
    {
        return false;
    }
    var key = Math.floor(Math.random() * tmp_o[sub].length);
    return [tmp_c[sub][key], tmp_o[sub][key]];
}

var listen = {};

function response(room, msg, sender, isGroupChat, replier, imageDB)
{
    room = room.trim();
	msg = msg.trim();
    if(listen[room])
    {
        if(listen[room] == msg)
        {
            replier.reply(sender + "님 정답!");
            listen[room] = false;
        }
        return;
    }
    msg = msg.split(" ");
    if(msg[0] == "/초성퀴즈")
    {
        if(!msg[1])
        {
            replier.reply("사용법: /초성퀴즈 {주제}");
            return;
        }
        let quiz = getQuiz(msg[1]);
        if(!quiz)
        {
            replier.reply("없는 주제입니다.");
        }
        else
        {
            replier.reply("[초성퀴즈] " + quiz[0]);
            listen[room] = quiz[1];
        }
    }
}

(function ()
{
    const initialConstants = ["ㄱ","ㄲ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅆ","ㅇ","ㅈ","ㅉ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"];

    Object.defineProperty(String.prototype, 'toInitialConstant',
    {
        'value': function ()
        {
            var ret = "";
            for(let i = 0; i < this.length; i++)
            {
                let code = this.charCodeAt(i);
                if(code < 44032 || code > 55203)
                {
                    ret += String.fromCharCode(code);
                }
                else
                {
                    ret += initialConstants[Math.floor((code - 44032) / 588)];
                }
            }
            return ret;
        }
    });

    for(var key in tmp_o)
    {
        if(!tmp_o.hasOwnProperty(key))
        {
            continue;
        }
        tmp_c[key] = [];
        for(let i = 0; i < tmp_o[key].length; i++)
        {
            tmp_c[key].push(tmp_o[key][i].toInitialConstant());
        }
    }
})();
