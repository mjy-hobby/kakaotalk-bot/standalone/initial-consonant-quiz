# 초성퀴즈

초성 퀴즈를 내고, 채점까지 하는 봇



## License

이 코드를 사용하려면 **봇 정보를 표시하는 기능을 수정하거나 제거하지 않아야만** 합니다.

위 사항만 지키면 누구나 어떤 용도로도 자유롭게 수정/배포/사용할 수 있습니다.

직·간접적인 어떠한 형태의 모든 보증을 부인합니다.



## Getting Started

봇을 설정하는 방법입니다.



### Prerequisites

카카오톡 봇입니다. 카카오톡이 설치되어 있어야 합니다.

듀얼 메신저는 동작하지만 `Parallel Space` 등으로는 사용할 수 없습니다.

봇 앱을 아무거나 설치해 주세요. (파일 경로가 [이 앱](https://play.google.com/store/apps/details?id=be.zvz.newskbot)을 기준으로 만들어졌습니다.)

[Wear OS by Google](https://play.google.com/store/apps/details?id=com.google.android.wearable.app)을 설치해 주세요. (설치가 되어 있어야 봇이 작동합니다.)

봇 앱에 알림 읽기 권한을 허용해 주세요. (알림을 읽어서 답장하는 원리입니다.)



### Setting

주제를 원하는대로 추가할 수 있~~게 만들겠~~습니다!



## Deployment

봇 사용법



### 게임 시작

`/초성퀴즈` : 봇의 사용법을 표시합니다.

`/초성퀴즈 {주제}` : 해당 주제로 초성퀴즈를 시작합니다.

※ 주제 목록 기능은 없습니다!



### 게임 방법

그 주제의, 그 초성이 나타내는 단어를 맞춰보세요!

※ 방 이름이 겹치면... 재미있는 일이 일어날 수 있습니다.



## Contributing

어느 정도 되기 전까지는 안 받습니다. (아직...)

어느 정도 된 다음부터는 간단한 오탈자 수정이라도 Pull Request는 환영합니다.

```C
if(cond)
{
    something();
}
```

괄호 닫고 중괄호 열기 전에, _띄어쓰기 아니고 줄바꿈입니다!_

인덴트는 무조건 띄어쓰기 4개로 해 주세요.



## Authors

- **맹주영** - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgments

- ... 흠